/*Inserts a value in CustID from Tickets that cannot be referenced to a value in Customers
Includes TicketID due to PrimaryKey*/
INSERT INTO Tickets (TicketID, CustID)
VALUES (456 ,200);
---------------------------------------
/*Inserts a value in AirlineID from Flights that cannot be referenced to a value in Airlines
Includes FlightNum due to PrimaryKey*/
INSERT INTO Flights (AirlineID, FlightNum)
VALUES ('AAA', 789);
---------------------------------------
/*Inserts valeus into AirlineID and FlightNum from tickets that cannot be referenced to their 
equivalents in Flights. Includes TicketID and CustID due to PrimaryKey and Unique*/
INSERT INTO Tickets (TicketID, CustID, AirlineID, FlightNum)
VALUES (765, 867, 'AAA', 500);
---------------------------------------
/*Tests a proper (positive) and improper (negative) input for the Cost constraint*/
UPDATE Tickets
SET Cost = 5;

UPDATE Tickets
SET Cost = -5;
---------------------------------------
/*Tests for proper (departure<arrival) and improper (departure>arrival) inputs*/
UPDATE Flights
SET DepartureTime = '12:00:00', ArrivalTime = '13:00:00';

UPDATE Flights
SET DepartureTime = '13:00:00', ArrivalTime = '12:00:00';
---------------------------------------
/*Tests for proper (State is TN, City of Knoxville) and improper (State is TN, City is anything else) inputs*/
UPDATE Airports
SET State = 'TN', City = 'Knoxville';

UPDATE Airports
SET State = 'TN', City = 'Placeholder';
---------------------------------------
/*Tests for proper (Origin <> Destination) vs improper (Origin = Destination) inputs
The where clause is due to the tuples being UNIQUE*/
UPDATE Flights
SET Origin = 'BFL', Destination = 'COS'
WHERE AirlineID = 'VRD' AND FlightNum = '120';

UPDATE Flights
SET Origin = 'BFL', Destination = 'BFL'
WHERE AirlineID = 'VRD' AND FlightNum = '120';