/*Creates an index for AirlineID and FlightnNum in Tickets*/
CREATE INDEX LookUpFlightTickets
ON Tickets (AirlineId, FlightNum);