/*Updates Tickets if there are any matching cases in NewTickets
by updating the SeatNum with the NT version and setting Paid to false*/
UPDATE Tickets AS T
SET SeatNum = NT.SeatNum, Paid = FALSE
FROM NewTickets AS NT
WHERE 
	T.TicketID = NT.TicketID AND
	T.CustID = NT.CustID AND
	T.AirlineID = NT.AirlineID AND
	T.FlightNum = NT.FlightNum;


/*Inserts NewTickets into Tickets if NewTicket's primary key is not in Ticket.
Cost and Paid are set to NULL and False respectively..*/
INSERT INTO Tickets(TicketID, CustID, AirlineID, FlightNum, SeatNum, Cost, Paid)
SELECT NT.TicketID, NT.CustID, NT.AirlineID, NT.FlightNum, NT.SeatNum, NULL, FALSE
FROM NewTickets AS NT
WHERE NT.TicketID NOT IN (SELECT TicketID FROM Tickets);
