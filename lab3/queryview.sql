/*Select from the passengers view from before the ID, Name, and number of flights*/
SELECT p.CustID, c.CustName, COUNT(*) AS CA_NY_Total
FROM CA_NY_Passengers p, Customers c
WHERE c.CustID = p.CustID
GROUP BY p.CustID, c.CustName;
/*Results:
 custid |    custname    | ca_ny_total
--------+----------------+-------------
    114 | Jim Halpert    |           1
    131 | Harvey Spectre |           1
    137 | Drew Powell    |           2
    139 | Sadik Hadzovic |           2*/

/*Delete Statement:
DELETE FROM Tickets WHERE TicketID = 202
DELETE FROM Tickets WHERE TicketID = 204*/

/*Results Post Deletes:
custid |    custname    | ca_ny_total
--------+----------------+-------------
    114 | Jim Halpert    |           1
    137 | Drew Powell    |           1
    139 | Sadik Hadzovic |           2*/