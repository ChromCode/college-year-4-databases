/*Adds a constraint that requires the cost in Tickets to be positive*/
ALTER TABLE Tickets
ADD CHECK (Cost>=1);


/*A constraint so the arrival time is always greater than departure.
Also names the constraint flights_take_time*/
ALTER TABLE Flights
ADD CONSTRAINT flights_take_time CHECK (ArrivalTime > DepartureTime);


/*If the state is TN, the city is knoxville. Else the state is not TN*/
ALTER TABLE Airports
ADD CHECK ((State = 'TN' AND City='Knoxville')OR State <> 'TN');


/*In flights origin and destiation cannot be the same*/
ALTER TABLE Flights
ADD CHECK (Origin <> Destination);