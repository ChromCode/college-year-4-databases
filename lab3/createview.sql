/*Create a view where airport ID connects to Flights origins and destinations
the airport states are defined to CA and ny, and then flights are connected to tickets*/
CREATE VIEW CA_NY_Passengers AS
SELECT t.CustID, t.AirlineID, t.FlightNum
FROM Tickets t, Airports a1, Airports a2, Flights fl
WHERE a1.AirportID = fl.Origin AND a2.AirportID = fl.Destination AND
a1.State = 'CA' AND a2.State = 'NY' AND 
fl.AirlineID = t.AirlineID AND fl.FlightNum = t.FlightNum;