/*The CustID in Tickets references the CustID in Customers*/
ALTER TABLE Tickets
ADD FOREIGN KEY (CustID) REFERENCES Customers(CustID);


/*The AirlineID in Flights references the AirlineID in Airlines*/
ALTER TABLE Flights
ADD FOREIGN KEY (AirlineID) REFERENCES Airlines(AirlineID);


/*The AirlineID and FlightNUM in Tickets references the same from Flights*/
ALTER TABLE Tickets
ADD FOREIGN KEY (AirlineID, FlightNum) REFERENCES Flights(AirlineID, FlightNum);