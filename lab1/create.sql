-- create.sql
-- Eric Poels
-- Student ID: 1421365
-- Lab 1
-- Script to create tables for lab1

DROP SCHEMA Lab1 CASCADE;
CREATE SCHEMA Lab1;

CREATE TABLE Airlines (
AirlineID CHAR(3) PRIMARY KEY,
AirlineName VARCHAR(30)
);

CREATE TABLE Airports (
AirportID CHAR(3) PRIMARY KEY,
City VARCHAR(30),
State VARCHAR(30)
);

CREATE TABLE Flights (
AirlineID CHAR(3),
FlightNum INTEGER,
Origin CHAR(3),
Destination CHAR(3),
DepartureTime TIME,
ArrivalTime TIME,
PRIMARY KEY(AirlineID, FlightNum)
);

CREATE TABLE Customers (
CustID INTEGER PRIMARY KEY,
CustName VARCHAR(30),
Status CHAR(1)
);

CREATE TABLE Tickets (
TicketID INTEGER PRIMARY KEY,
CustID INTEGER,
AirlineID CHAR(3),
FlightNum INTEGER,
FlightDate DATE,
SeatNum CHAR(3),
Cost DECIMAL,
Paid BOOLEAN
);
